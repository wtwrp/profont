#!/usr/bin/env python
if __name__ == '__main__':
	import sys
	import re
	from os.path import basename
	from convert import convert
	from PIL import Image

	outputName = sys.argv[1]

	glyphmap = {}
	for filename in sys.argv[2:]:
		m = re.search(r"^([0-9a-fA-F]+).+", basename(filename))
		code = int(m.group(1), 16)
		bitmap = Image.open(filename)
		glyphmap[code] = bitmap

	convert(glyphmap, outputName)
